//ToDo: arithmetic operations
/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a,b) => a + b;

const subtract =(minuend, subtrahend) => {
    return minuend - subtrahend;
};
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
}
/**
 * Divides two numbers.
 * @param {number} didend 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor ==0) throw new Error("0 division not allowed");
    try {
        const fraction = dividend / divisor;
        return fraction;
    }  catch (err) {
        throw new Error("0 division now allowed");
    }
};

export default { add, subtract, multiply, divide,  }